const MNEMONIC = "shuffle meadow link purity walnut foster roof option soccer quick name lucky";
const rinkebyNodeUrl = "https://rinkeby.infura.io/v3/2fdc18d245944c3a980d334411c78333";
var HDWalletProvider = require("@truffle/hdwallet-provider");

module.exports = {

  networks: {
    rinkeby: {
      provider: function () {
        return new HDWalletProvider(MNEMONIC, rinkebyNodeUrl);
      },
      gas: 5000000,
      skipDryRun: true,
      network_id: "*",
    },
  },

  compilers: {
    solc: {
      version: "0.7.6",
      settings: {
        optimizer: {
          enabled : true,
          runs: 200
        },
        evmVersion: "istanbul"
      }
    }
  }
};
